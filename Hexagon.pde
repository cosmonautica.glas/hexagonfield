class Hexagon{

int sides = 6;
float x_position = 0;
float y_position = 0;
float radius = 23;
float lightintensity = 0;
int id = 0;
boolean id_visible = false;
boolean is_visible = true;


//Constructor
/*
Hexagon(int _id, float _x, float _y){
  id = _id;
  x_position = _x;
  y_position = _y;

}
*/
Hexagon(float _x, float _y){
  x_position = _x;
  y_position = _y;
}

Hexagon(int _id){
  id = _id;
}

void increase_lightintensity(){
  if(lightintensity < 255){
    lightintensity = lightintensity +1;
  }
}

void decrease_lightintensity(){
  if(lightintensity > 0){
    lightintensity = lightintensity -7;
  }
  if(lightintensity <0){lightintensity = 0;}
}


void draw_hexagon(){
  pushMatrix();
  fill(lightintensity);
  polygon(x_position, y_position, radius, sides);

  popMatrix();
  pushMatrix();
  fill(255,255,255);
  if (id_visible == true) {
    show_id_number();
  }
  popMatrix();
}

void show_id_number() {
  textSize(25);
  text(id, x_position-15, y_position+5);
  }

void polygon(float x, float y, float radius, int npoints) {
  float angle = TWO_PI / npoints;
  beginShape();
  for (float a = 0; a < TWO_PI; a += angle) {
    float sx = x + cos(a) * radius;
    float sy = y + sin(a) * radius;
    vertex(sx, sy);
  }
  endShape(CLOSE);
  }

void translate(float _x, float _y){
  x_position = _x;
  y_position = _y;
  return;
  }

}
